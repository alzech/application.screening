﻿using application.screening.core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace application.screening.bll.Managers
{
    public interface IApplicationManager
    {
        /// <summary>
        /// Gets thespecified application.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>An application including the applicantc name and answers to standard questions.</returns>
        Application GetApplication(int id);

        /// <summary>
        /// Adds a application to the system, first determining if it is considered acceptable. Applications are acceptable if all questiosn are answered correctly.
        /// </summary>
        /// <param name="name">The applicatnts name</param>
        /// <param name="answers">A list of answers to standard questions.</param>
        /// <returns>The newly added application.</returns>
        Application AddApplication(string name, IEnumerable<ApplicantAnswer> answers);

        /// <summary>
        /// Gets a list of acceptable applications.
        /// </summary>
        /// <returns>a list of applications, including the applicants name and list of answers.</returns>
        IEnumerable<Application> GetAcceptableApplications();

        /// <summary>
        /// Gets the list of standard application questions and the correct answers.
        /// </summary>
        /// <returns>A list of standard application questions and the correct answers.</returns>
        bool IsApplicationAcceptable(IEnumerable<ApplicantAnswer> answers);

        /// <summary>
        /// Determines whether an application is considered acceptable, if all answers match the correct answers.
        /// </summary>
        /// <param name="answers">The answers.</param>
        /// <returns>
        ///   <c>true</c> if all all questions answered and all answers match the correct answers  otherwise, <c>false</c>.
        /// </returns>
        IEnumerable<ApplicationQuestion> GetApplicationQuestions();

    }
}
