﻿using application.screening.core.Data;
using application.screening.core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace application.screening.bll.Managers
{
    public class ApplicationManager : IApplicationManager
    {
        private IApplicationRepository _appRepo;
        private IApplicationQuestionRepository _appQuestionRepo;


        public ApplicationManager(IApplicationRepository appRepo,
                                  IApplicationQuestionRepository appQuestionRepo)
        {
            _appRepo = appRepo;
            _appQuestionRepo = appQuestionRepo;
        }

        /// <summary>
        /// Gets thespecified application.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>An application including the applicantc name and answers to standard questions.</returns>
        public Application GetApplication(int id)
        {
            return _appRepo.FetchApplicationById(id);
        }

        /// <summary>
        /// Adds a application to the system, first determining if it is considered acceptable. Applications are acceptable if all questiosn are answered correctly.
        /// </summary>
        /// <param name="name">The applicatnts name</param>
        /// <param name="answers">A list of answers to standard questions.</param>
        /// <returns>The newly added application.</returns>
        public Application AddApplication(string name, IEnumerable<ApplicantAnswer> answers)
        {
            //determine if application is acceptable 
            var isAcceptable = IsApplicationAcceptable(answers);
            var newApp = new Application()
            {
                Name = name,
                IsAcceptable = isAcceptable,
                Questions = answers
            };
            var addedApp = _appRepo.AddApplication(newApp);
            return addedApp;
        }

        /// <summary>
        /// Gets a list of acceptable applications.
        /// </summary>
        /// <returns>a list of applications, including the applicants name and list of answers.</returns>
        public IEnumerable<Application> GetAcceptableApplications()
        {
            return _appRepo.FetchAllAcceptableApplications();
        }

        /// <summary>
        /// Gets the list of standard application questions and the correct answers.
        /// </summary>
        /// <returns>A list of standard application questions and the correct answers.</returns>
        public IEnumerable<ApplicationQuestion> GetApplicationQuestions()
        {
            return _appQuestionRepo.FetchAllQuestions();
        }

        /// <summary>
        /// Determines whether an application is considered acceptable, if all answers match the correct answers.
        /// </summary>
        /// <param name="answers">The answers.</param>
        /// <returns>
        ///   <c>true</c> if all all questions answered and all answers match the correct answers  otherwise, <c>false</c>.
        /// </returns>
        public bool IsApplicationAcceptable(IEnumerable<ApplicantAnswer> answers)
        {
            var allQuestions = _appQuestionRepo.FetchAllQuestions();
            //check that the application has acceptable answers for all questions
            foreach (var q in allQuestions)
            {
                var appAnswer = answers.FirstOrDefault(x => x.QuestionId == q.Id);
                //check if question answered
                if (appAnswer == null)
                    return false;
                //check id answer is accptable
                if (!string.Equals(appAnswer.Answer.Trim(), q.Answer.Trim(), StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }
            return true;
        }
    }
}