using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.DataHandler.Serializer;
using Microsoft.Owin.Security.DataProtection;

[assembly: WebActivator.PostApplicationStartMethod(typeof(application.screening.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace application.screening.App_Start
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using dal.Repositories;
    using bll.Managers;

    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {

            //Repositories
            var repositoryAssembly = typeof(ApplicationRepository).Assembly;
            var repoRegistrations =
                from type in repositoryAssembly.GetExportedTypes()
                where type.Namespace == "application.screening.dal.Repositories"
                where type.GetInterfaces().Any()
                select new { Service = type.GetInterfaces().Single(), Implementation = type };

            foreach (var reg in repoRegistrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }


            //Managers
            var managerAssembly = typeof(ApplicationManager).Assembly;
            var managerRegistrations =
                from type in managerAssembly.GetExportedTypes()
                where type.Namespace == "application.screening.bll.Managers"
                where type.GetInterfaces().Any()
                select new { Service = type.GetInterfaces().Single(), Implementation = type };

            foreach (var reg in managerRegistrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }
        }
    }
}