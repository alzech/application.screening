﻿using application.screening.Models;
using System.Linq;
using System.Web.Http;
using application.screening.bll.Managers;
using application.screening.core.Data;

namespace application.screening.Controllers
{
    [RoutePrefix("api/application")]
    public class ApplicationController : ApiController
    {
        private readonly IApplicationManager _appManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationController"/> class.
        /// </summary>
        /// <param name="appManager">The application manager.</param>
        public ApplicationController(IApplicationManager appManager)
        {
            _appManager = appManager;
        }

        /// <summary>
        /// Gets the application for the specified id. 
        /// </summary>
        /// <param name="id">The id of the application.</param>
        /// <returns>The application including the aplicants name and answers to a standard set of questions.</returns>
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(_appManager.GetApplication(id));
        }

        /// <summary>
        /// Gets the list of stanard questions for an application.
        /// </summary>
        /// <returns>A list of questions.</returns>
        [Route("questions")]
        [HttpGet]
        public IHttpActionResult GetQuestions()
        {
            var questions = _appManager.GetApplicationQuestions();
            return Ok(questions.Select(x => new { QuestionId = x.Id, Question = x.Question }));
        }

        /// <summary>
        /// Submits an application to the system. The application must include the applicatnts name and the answers to a standard list of questions.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>The newly submitted application</returns>
        [HttpPost]
        public IHttpActionResult Post(AddApplicationModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var newApp = _appManager.AddApplication(model.Name, model.Questions.Select(x => new ApplicantAnswer()
            {
                QuestionId = x.QuestionId,
                Answer = x.Answer
            }));
            return Ok(newApp);
        }

        

    }
}
