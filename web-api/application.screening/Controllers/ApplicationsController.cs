﻿using application.screening.bll.Managers;
using System.Web.Http;

namespace application.screening.Controllers
{

    [RoutePrefix("api/applications")]
    public class ApplicationsController : ApiController
    {
        private readonly IApplicationManager _appManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationsController"/> class.
        /// </summary>
        /// <param name="appManager">The application manager.</param>
        public ApplicationsController(IApplicationManager appManager)
        {
            _appManager = appManager;
        }

        /// <summary>
        /// Returns a list of acceptable applications. including the application id and applicants name.
        /// </summary>
        /// <returns>A list of acceptable applications,  including the application id and applicants name.</returns>
        [Route("accepted")]
        [HttpGet]
        public IHttpActionResult GetAccepted()
        {
            return Ok(_appManager.GetAcceptableApplications());
        }
    }
}