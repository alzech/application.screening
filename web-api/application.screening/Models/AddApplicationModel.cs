﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.screening.Models
{
    public class AddApplicationModel
    {
        [Required]
        public string Name { get; set; }
        public IEnumerable<QuestionModel> Questions { get; set; }
    }

    public class QuestionModel
    {
        [Required]
        public string QuestionId { get; set; }
        [Required]
        public string Answer { get; set; }
    }
}