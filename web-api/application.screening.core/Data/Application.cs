﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.core.Data
{
    /// <summary>
    /// A complete application
    /// </summary>
    public class Application
    {
        //the id of the application
        public int Id { get; set; }
        //the name of the applicant
        public string Name { get; set; }
        //if the application is considered acceptable
        public bool IsAcceptable { get; set; }
        //a list of questions and answers
        public IEnumerable<ApplicantAnswer> Questions { get; set; }
    }

    public class ApplicantAnswer
    {
        // the id of the application question
        public string QuestionId { get; set; }
        //the answer given for the question
        public string Answer { get; set; }
    }
}