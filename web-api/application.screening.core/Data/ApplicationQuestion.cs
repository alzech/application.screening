﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.core.Data
{
    //a list of standard questions to determine if an application is acceptable
    public class ApplicationQuestion
    {
        //the id of the question
        public string Id { get; set; }
        //the question
        public string Question { get; set; }
        //the correct answer
        public string Answer { get; set; }
    }
}