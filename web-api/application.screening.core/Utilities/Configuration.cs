﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace application.screening.core.Utilities
{
    public class Configuration
    {
        public static string ConnectionStringName = "DefaultConnection";
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}