﻿using application.screening.core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.core.Repositories
{
    public interface IApplicationQuestionRepository
    {
        /// <summary>
        /// Gets an application question by id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The application question.</returns>
        ApplicationQuestion FetchQuestionById(int id);

        /// <summary>
        /// Gets all avaliable applicatin questions with acceptables answers.
        /// </summary>
        /// <returns>A enumeration of application questions and answers.</returns>
        IEnumerable<ApplicationQuestion> FetchAllQuestions();
    }
}