﻿using application.screening.core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace application.screening.core.Repositories
{
    public interface IApplicationRepository
    {
        /// <summary>
        /// Fetches the application by identifier.
        /// </summary>
        /// <param name="id">The application id.</param>
        /// <returns></returns>
        Application FetchApplicationById(int id);

        /// <summary>
        /// Fetches all applications.
        /// </summary>
        /// <returns>a list of all application sin the system</returns>
        IEnumerable<Application> FetchAllApplications();

        /// <summary>
        /// Fetches all acceptable applications.
        /// </summary>
        /// <returns>A list of all acceptable applicatio in the system</returns>
        IEnumerable<Application> FetchAllAcceptableApplications();

        /// <summary>
        /// Adds the application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The newly added application.</returns>
        Application AddApplication(Application app);
    }
}
