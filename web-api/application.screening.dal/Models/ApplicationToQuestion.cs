﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.dal.Models
{
    [PrimaryKey("Id")]
    public class ApplicationToQuestion
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string QuestionId { get; set; }
        public string Answer { get; set; }
    }
}