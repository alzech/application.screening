﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.dal.Models
{
    [PrimaryKey("Id")]
    public class Application
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAcceptable { get; set; }
    }
}