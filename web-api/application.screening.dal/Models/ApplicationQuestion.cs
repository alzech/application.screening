﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.dal.Models
{
    public class ApplicationQuestion
    {
        
        public string Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}