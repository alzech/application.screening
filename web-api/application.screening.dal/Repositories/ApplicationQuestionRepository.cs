﻿using application.screening.core.Repositories;
using application.screening.core.Utilities;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.dal.Repositories
{
    public class ApplicationQuestionRepository : IApplicationQuestionRepository
    {
        private Database _db; 
        public ApplicationQuestionRepository()
        {
            _db = new Database(Configuration.ConnectionStringName);
        }

        /// <summary>
        /// Gets an application question by id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The application question.</returns>
        public core.Data.ApplicationQuestion FetchQuestionById(int id)
        {
            Models.ApplicationQuestion question = _db.SingleOrDefault<Models.ApplicationQuestion>(id);
            return new core.Data.ApplicationQuestion()
            {
                Id = question.Id,
                Question = question.Question,
                Answer = question.Answer
            };
        }

        /// <summary>
        /// Gets all avaliable applicatin questions with acceptables answers.
        /// </summary>
        /// <returns>A enumeration of application questions and answers.</returns>
        public IEnumerable<core.Data.ApplicationQuestion> FetchAllQuestions()
        {
            IEnumerable<Models.ApplicationQuestion> questions = _db.Fetch<Models.ApplicationQuestion>("");
            return questions.Select(q => new core.Data.ApplicationQuestion()
            {
                Id = q.Id,
                Question = q.Question,
                Answer = q.Answer
            });
        }
    }
}