﻿using application.screening.core.Repositories;
using application.screening.core.Utilities;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.screening.dal.Repositories
{
    public class ApplicationRepository : IApplicationRepository
    {
        private Database _db;
        public ApplicationRepository()
        {
            _db = new Database(Configuration.ConnectionStringName);
        }
        /// <summary>
        /// Fetches the application by identifier.
        /// </summary>
        /// <param name="id">The application id.</param>
        /// <returns></returns>
        public core.Data.Application FetchApplicationById(int id)
        {
            Models.Application app = _db.SingleOrDefault<Models.Application>(id);
            IEnumerable<Models.ApplicationToQuestion> appAnswers = _db.Fetch<Models.ApplicationToQuestion>("WHERE ApplicationId = @0", id);
            return new core.Data.Application()
            {
                Id = app.Id,
                Name = app.Name,
                IsAcceptable = app.IsAcceptable,
                Questions = appAnswers.Select(a => new core.Data.ApplicantAnswer()
                {
                    QuestionId = a.QuestionId,
                    Answer = a.Answer
                })
            };
        }
        /// <summary>
        /// Fetches all applications.
        /// </summary>
        /// <returns>a list of all application sin the system</returns>
        public IEnumerable<core.Data.Application> FetchAllApplications()
        {
            IEnumerable<Models.Application> apps = _db.Fetch<Models.Application>("");
            return apps.Select(a => new core.Data.Application()
            {
                Id = a.Id,
                IsAcceptable = a.IsAcceptable,
                Name = a.Name
            });
        }
        /// <summary>
        /// Fetches all acceptable applications.
        /// </summary>
        /// <returns>A list of all acceptable applicatio in the system</returns>
        public IEnumerable<core.Data.Application> FetchAllAcceptableApplications()
        {
            IEnumerable<Models.Application> apps = _db.Fetch<Models.Application>("WHERE IsAcceptable = @0", 1);
            return apps.Select(a => new core.Data.Application()
            {
                Id = a.Id,
                IsAcceptable = a.IsAcceptable,
                Name = a.Name
            });
        }
        /// <summary>
        /// Adds the application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>The newly added application.</returns>
        public core.Data.Application AddApplication(core.Data.Application app)
        {
            IList<core.Data.ApplicantAnswer> insertedAnswers = new List<core.Data.ApplicantAnswer>() {};
            Models.Application newApp = new Models.Application()
            {
                Name = app.Name,
                IsAcceptable = app.IsAcceptable
            };
            using (ITransaction scope = _db.GetTransaction())
            {
                _db.Insert(newApp);
                if (app.Questions != null && app.Questions.Any())
                    foreach (var question in app.Questions)
                    {
                        Models.ApplicationToQuestion newAnswer= new Models.ApplicationToQuestion()
                        {
                            ApplicationId = newApp.Id,
                            QuestionId = question.QuestionId,
                            Answer = question.Answer
                        };
                        _db.Insert(newAnswer);
                        insertedAnswers.Add(new core.Data.ApplicantAnswer()
                        {
                            QuestionId = newAnswer.QuestionId,
                            Answer = newAnswer.Answer
                        });
                    }
                scope.Complete();
                return new core.Data.Application()
                {
                    Id = newApp.Id,
                    IsAcceptable = newApp.IsAcceptable,
                    Name = newApp.Name,
                    Questions = insertedAnswers
                };
            }
        }
    }
}