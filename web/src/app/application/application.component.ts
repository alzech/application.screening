import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
// services
import { WorkerDataService, IApplicationQuestion, IApplication } from '../worker-data.service';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {

  app: IApplication = {};
  // questions: any;

  constructor(private dataService: WorkerDataService,
              private router: Router) { }

  ngOnInit() {
    this.dataService.getApplicationQuestions()
      .subscribe(data => {
          // t//his.questions = res;
          console.log('get application questions', data);
          for (let q of data) {
            this.app.Questions.push( {QuestionId : q.QuestionId, Answer: undefined, Question: q.Question} );
          }
      }, error => console.log('Error', error));
    this.app.Questions = new Array();

  }

onSubmit() {
  this.dataService.addApplication(this.app)
    .subscribe(data => {
      this.router.navigate(['/applications']);
    }, error => console.log('Error', error));
}

}
