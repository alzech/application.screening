import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, RequestOptions, Headers } from '@angular/http';
 import {Observable} from 'rxjs/Rx';
 import 'rxjs/add/operator/map';



 export interface IApplicant {
  Id: number;
  Name: string;
}

export interface IApplication {
  Id?: number;
  Name?: string;
  Questions?: IApplicationQuestion[];
}

/* export interface IApplicationAnswer {
  QuestionId: number;
  Answer: string;
} */

export interface IApplicationQuestion {
  QuestionId: number;
  Question: string;
  Answer?: string;
}

@Injectable()
export class WorkerDataService {

  options: RequestOptionsArgs;
  baseUrl: string;

  constructor(private http: Http) {
      this.options = new RequestOptions();
      this.options.headers = new Headers();
      this.options.headers.append('Content-Type', 'application/json');
      this.options.headers.append('Accept', 'application/json');

      this.baseUrl = 'http://localhost:58406/api';

  }

  getAcceptableApplications() {
    return this.http.get(`${this.baseUrl}/applications/accepted`, this.options)
    .map((res: Response) => res.json());
  }

  addApplication(model: IApplication) {
    return this.http.post(`${this.baseUrl}/application`, model);
  }

  getApplicationQuestions() {
    return this.http.get(`${this.baseUrl}/application/questions`, this.options)
    .map((res: Response) => res.json());
  }
}
