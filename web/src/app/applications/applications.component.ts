import { Component, OnInit } from '@angular/core';
// services
import { WorkerDataService, IApplicant } from '../worker-data.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  applicants: IApplicant[] = [];

  constructor(private dataService: WorkerDataService) { }

  ngOnInit() {
    this.getApplications();
  }

  getApplications() {
    this.dataService.getAcceptableApplications()
    .subscribe(data => {
        this.applicants = data;
        console.log('get acceptable applications', data, this.applicants);
    }, error => console.log('Error', error));
  }
}
