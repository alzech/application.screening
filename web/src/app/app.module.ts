import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { WorkerDataService } from './worker-data.service';
import { AppComponent } from './app.component';
import { ApplicationsComponent } from './applications/applications.component';
import { ApplicationComponent } from './application/application.component';
import { FormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: '', redirectTo: 'applications', pathMatch: 'full' },
  { path: 'applications', component: ApplicationsComponent },
  { path: 'application', component: ApplicationComponent }//,
  //{ path: 'selection', component:SelectionScreenComponent},
  //{ path: 'selectionDetails/:id', component:SelectionDeatilsScreenComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ApplicationsComponent,
    ApplicationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  providers: [WorkerDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
