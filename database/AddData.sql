INSERT INTO ApplicationQuestion(Id, Question, Answer)
VALUES('Id1', 'What color is the sky?', 'blue')
GO
INSERT INTO ApplicationQuestion(Id,Question, Answer)
VALUES('Id2', 'What color is grass?', 'green')
GO
INSERT INTO ApplicationQuestion(Id,Question, Answer)
VALUES('Id3', 'What color is the sun?', 'yellow')
GO

INSERT INTO [Application](Name, IsAcceptable)
VALUES('Mary Smith', 1)
GO
INSERT INTO [Application](Name, IsAcceptable)
VALUES('Joe Brown', 0)
GO
INSERT INTO [Application](Name, IsAcceptable)
VALUES('Henry Jones', 1)
GO

INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id1', 1, 'blue')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id2', 2, 'blue')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id3', 3, 'blue')

INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id1', 1, 'green')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id2', 2, 'green')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id3', 3, 'green')

INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id1', 1, 'yellow')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id2', 2, 'purple')
INSERT INTO ApplicationToQuestion(QuestionId,ApplicationId,Answer)
VALUES('Id3', 3, 'yellow')