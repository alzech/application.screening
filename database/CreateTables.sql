CREATE TABLE [Application]
(
	[Id] int NOT NULL IDENTITY (1, 1),
	[Name] varchar(250) NOT NULL,
	[IsAcceptable] bit NOT NULL DEFAULT (0)
)

GO

CREATE TABLE [ApplicationQuestion]
(
	[Id] varchar(max) NOT NULL,
	[Question] varchar(max) NOT NULL,
	[Answer]  varchar(max) NOT NULL
)

GO

CREATE TABLE [ApplicationToQuestion]
(
	[Id] int NOT NULL IDENTITY (1, 1),
	[QuestionId] varchar(max) NOT NULL,
	[ApplicationId] int NOT NULL,
	[Answer] varchar(max) NOT NULL 
)

